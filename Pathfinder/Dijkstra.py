import copy


class Node:
    key = ""
    adjacents = {}
    distance = 20000000000
    visited = False
    previous = None

    def __init__(self, key):
        self.key = key
        self.adjacents = {}
        self.visited = False

    def reset(self):
        self.visited = False
        self.distance = 20000000000
        self.visited = False
        self.previous = None

    def add_branch(self, other, time):
        self.adjacents[other] = time

    def get_connections(self):
        return self.adjacent.keys()

    def get_time(self, other):
        return self.adjacents[other]

    def get_distance(self):
        return self.distance

    def set_distance(self, value):
        self.distance = value

    def get_key(self):
        return self.key

    def set_visited(self):
        self.visited = True

    def set_previous(self, prev):
        self.previous = prev

    def __lt__(self, other):
        return self.distance < other.distance

    def __cmp__(self, other):
        if self.distance < other.distance:
            return -1
        elif self.distance > other.distance:
            return 1
        else:
            return 0

    def __str__(self):
        return str(self.key) + ' adjacent: ' + str([x.key for x in self.adjacents])


def create_graph(dataframe):
    graph = []
    for _, row in dataframe.iterrows():
        start = row["start"]
        destination = row["destination"]
        duree = row["duree"]

        # check for duplicate before insertion
        if not any(x.key == start for x in graph):
            graph.append(Node(start))
        if not any(x.key == destination for x in graph):
            graph.append(Node(destination))

        # add branches for both directions
        start_node = next(x for x in graph if x.key == start)
        destination_node = next(x for x in graph if x.key == destination)
        start_node.add_branch(destination_node, duree)
        destination_node.add_branch(start_node, duree)
    return graph


def dij(graph, start, arr):
    import heapq
    result = None

    # Set the distance for the start node to zero

    # Put tuple pair into the priority queue
    start.set_distance(0)

    unvisited_queue = [(v.get_distance(), v) for v in graph]
    heapq.heapify(unvisited_queue)
    while len(unvisited_queue):
        # Pops a vertex with the smallest distance
        uv = heapq.heappop(unvisited_queue)
        current = uv[1]
        current.set_visited()

        # for next in v.adjacent:
        for next in current.adjacents:
            # if visited, skip
            if next.visited:
                continue
            new_dist = current.get_distance() + current.get_time(next)

            if new_dist < next.get_distance():
                next.set_distance(new_dist)
                next.set_previous(current)

        while len(unvisited_queue):
            heapq.heappop(unvisited_queue)
        # 2. Put all vertices not visited into the queue
        unvisited_queue = [(v.get_distance(), v) for v in graph if not v.visited]
        heapq.heapify(unvisited_queue)


def log(arr, dep):
    o = arr
    while o is not None:
        if o == dep:
            break
        o = arr.previous


def extract(nodes, city):
    res = []
    for x in nodes:
        if city in x.key:
            res.append(x)
    return res


def shortest(v, path):
    if v.previous:
        path.append(v.previous.get_key())
        shortest(v.previous, path)


def find_path(dataframe, start, destination):
    """

    """
    graph = create_graph(dataframe)

    start_cities = extract(graph, start)
    destination_cities = extract(graph, destination)

    res = None
    result = None
    for start in start_cities:
        for destination in destination_cities:

            for node in graph:
                node.reset()
            dij(graph, start, destination)

            target = destination
            path = [target.get_key()]
            shortest(target, path)
            if res is not None and res.get_distance() < target.get_distance():
                res = copy.copy(target)
                result = copy.copy(path[::-1])
            else:
                res = copy.copy(target)
                result = copy.copy(path[::-1])
                print('The shortest path : %s' % (path[::-1]))
                return result
    return result
