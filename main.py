from SpeechRecognition import listen
from DataExploitation import *
from Pathfinder import *
from TrainingModel import *


def main():
    data = extract_data("./timetables.csv")
    model = create_model("train_doc_epitech.csv", ",")
    
    nlp = nlp_with_entity_recognizer(model)
    nlp = train_nlp(nlp, model)
    while True:
        text = listen()
        print(text)
        destinations = get_destination(nlp, text)
        departs = get_depart(nlp, text)
        destination = destinations[0] if len(destinations) > 0 else ""
        depart = departs[0] if len(departs) > 0 else "Paris"
        if destination != "":
            find_path(data, destination, depart)
        else:
            print("Aucun trajet n'a pu être déterminé")


if __name__ == '__main__':
    main()
