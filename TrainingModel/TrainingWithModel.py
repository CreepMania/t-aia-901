import spacy
import pandas as pd

import plac
import random
from pathlib import Path


# transform a csv into a model for spacy
def create_model(path, sep, destination = "DESTINATION", depart = "DEPART", error = "ERROR"):

    csv = pd.read_csv(path, sep=sep)
    model = []
    index = 0
    for phrase  in csv.PHRASE :
        tmp = {}
        tmp['entities'] = []
        if (csv[error][index] == "FALSE" or csv[error][index] == "FAUX") and phrase.find(csv[destination][index]) != -1:
            tmpCell = phrase.find(csv[destination][index]), phrase.find(csv[destination][index]) + len(csv[destination][index]), 'destination'
            tmp['entities'].append(tmpCell)
            if csv[depart][index] != "none" and phrase.find(csv[depart][index]) != -1:
                tmpCell = phrase.find(csv[depart][index]), phrase.find(csv[depart][index]) + len(csv[depart][index]), "depart"
                tmp['entities'].append(tmpCell)

        cell = phrase, tmp
        model.append(cell)
        index += 1
    return model


def nlp_with_entity_recognizer(train_data):
    nlp = spacy.blank('fr')
    if 'ner' not in nlp.pipe_names:
        ner = nlp.create_pipe('ner')
        nlp.add_pipe(ner)

    for _, annotations in train_data:
        # Here we are only interested in label to be added (depart, destination)
        for _s, _e, label in annotations.get('entities', []) :
            ner.add_label(label)
    return nlp


def train_nlp(nlp, model):
    try:
        nlp.from_disk("./model");
        return nlp
    except:
        print("Model not found, starting training...")
    n_iter = 16
    other_pipes = [pipe for pipe in nlp.pipe_names if pipe != 'ner']
    with nlp.disable_pipes(*other_pipes):  # only train NER
        # Strart training here
        # optimizer function to update the model's weights. 
        optimizer = nlp.begin_training()
        for itn in range(n_iter):
            # At each iteration, the training data is shuffled to ensure the model 
            # doesn't make any generalisations based on the je order of examples
            random.shuffle(model)
            losses = {}
            for text, annotations in model:
                # "drop" is to improve the learning results, rate at which to randomly 
                # "drop" individual features and representations, making the model to
                # memorise the training data
                # sgd = Stochastic Gradient Descent, see https://en.wikipedia.org/wiki/Stochastic_gradient_descent
                nlp.update([text], [annotations], sgd=optimizer, drop=0.35,losses=losses)
            print('losses -', losses)
    nlp.to_disk("./model")
    return nlp


def get_destination(nlp ,text):
    doc = nlp(text)
    return [word.text for word in doc.ents if word.label_ == "destination"]


def get_depart(nlp, text):
    doc = nlp(text)
    return [word.text for word in doc.ents if word.label_ == "depart"]
