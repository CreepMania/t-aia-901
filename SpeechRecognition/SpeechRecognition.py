#!/usr/bin/env python3
import speech_recognition as sr


def listen():
    r = sr.Recognizer()
    sr.Microphone.list_microphone_names()
    with sr.Microphone() as source:
        print("Say something!")
        audio = r.listen(source)
    try:
        with open("output.txt", 'w') as file:
            result = r.recognize_google(audio, language='fr-FR')
            file.write(result)
            return result
    except sr.UnknownValueError:
        print("Google Speech Recognition could not understand audio")
    except sr.RequestError as e:
        print("Could not request results from Google Speech Recognition service; {0}".format(e))
