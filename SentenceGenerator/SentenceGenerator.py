import random
import sys
import enum

SOURCE_DIRECTORY = "./SentencesParts/"
OUTPUT_FILE = "./Result.txt"

def replacePronom(sentence, pronomline) :
    if "[pronom]" in sentence :
        with open(SOURCE_DIRECTORY + "PronomPersonnel.txt") as f:
            for i, l in enumerate(f):
                if (i == pronomline) :
                    pronom = l
                pass
        pronom = cutLineBreak(pronom)
        sentence = sentence.replace("[pronom]", pronom)
    return sentence

def getNumberOfLoop() :
    for arg in sys.argv :
        if (arg != "SentenceGenerator.py") :
            numberOfLoop = int(arg)
    return numberOfLoop

def fileLength(fileName):
    with open(SOURCE_DIRECTORY + fileName) as f:
        for i, l in enumerate(f):
            pass
    return i

def selectRandomItemInFile(fileName, line = -1) :
    if line == -1 :
        numberOfLine = fileLength(fileName)
        choosenIndexOfLine = random.randint(0, numberOfLine)
    else :
        choosenIndexOfLine = line

    with open(SOURCE_DIRECTORY + fileName) as f:
        for i, l in enumerate(f):
            if (i == choosenIndexOfLine) :
                line = l
            pass
    lineList = line.split("|")
    itemIndex = random.randint(0, len(lineList)-1)
    return lineList[itemIndex], choosenIndexOfLine

def cutLineBreak(sentence) :
    if (sentence[-1] == '\n' or sentence[-1] == ';' or sentence[-1] == '|' or sentence[-1] == ' ') :
        sentence = sentence[:-1]
    return sentence

def buildSentence() :
    sentence = ""
    sentencePiece, pronomLine = selectRandomItemInFile("Pronom.txt")
    sentence += sentencePiece
    sentence = cutLineBreak(sentence)
    sentence += " "
    sentencePiece, line = selectRandomItemInFile("Desir.txt", pronomLine)
    sentence += sentencePiece
    sentence = cutLineBreak(sentence)
    sentence += " "
    sentencePiece, actionLine = selectRandomItemInFile("Action.txt")
    sentence += sentencePiece
    sentence = cutLineBreak(sentence)
    sentence += " "
    firstTown, line = selectRandomItemInFile("Ville.txt")
    sentence += firstTown
    sentence = cutLineBreak(sentence)
    sentence += " "
    sentencePiece, line = selectRandomItemInFile("Liaison.txt", actionLine)
    sentence += sentencePiece
    sentence = cutLineBreak(sentence)
    sentence += " "
    secondTown, line = selectRandomItemInFile("Ville.txt")
    sentence += secondTown
    sentence = cutLineBreak(sentence)
    sentence = replacePronom(sentence, pronomLine)
    if actionLine == 0 :
        return sentence, firstTown, secondTown
    else :
        return sentence, secondTown, firstTown

def askIfSentenceIsOk(sentence) :
    while True :
        print(sentence + " | y, n or i ? :")
        userInput = input()
        if (userInput == "y" or userInput == "Y") :
            return 1
        elif (userInput == "n" or userInput == "N") :
            return 0
        elif (userInput == "i" or userInput == "I") :
            return -1

def writeToOutputFile(result) :
    file_object = open(OUTPUT_FILE, 'a')
    file_object.write(result)
    file_object.close()

def convertToError(isSentenceOk) :
    if (isSentenceOk == 1) :
        return "FAUX"
    else :
        return "VRAI"

numberOfLoop = getNumberOfLoop()
print(numberOfLoop)
for i in range(numberOfLoop) :
    sentence, arivalTown, departureTown = buildSentence()
    isSentenceOk = askIfSentenceIsOk(sentence)
    if isSentenceOk != -1 :
        error = convertToError(isSentenceOk)
        result = sentence + ";" + arivalTown + ";" + departureTown + ";" + error + "\n"
        writeToOutputFile(result)